<?php

namespace App\Controller;

use Symfony\Component\HttpClient\HttpClient;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ProfileController extends AbstractController
{
    /**
     * @Route("/profile")
     * @return Response
     * @throws \Exception
     */
    public function index()
    {
        $httpClient = HttpClient::create();

        $requestData = [
            't' => time(),
            'sig' => '00000000000000000000000000000000',
            'action' => 'getStatItemsDetails',
            'playerId' => $_GET['playerId'],
            'dateFrom' => (new \DateTime('6 months ago'))->getTimestamp(),
            'dateTo' => time(),
            'page' => $_GET['page'] ?? 0,
        ];

        $response = $httpClient->request('POST', 'https://vk-dev-15-app.supercitygame.com/api.php', ['body' => $requestData]);
        //$response = $httpClient->request('POST', 'http://localhost:8080/api.php', ['body' => $requestData]);
        $content = $response->toArray();

        return $this->render('profile/index.html.twig', [
            'page' => 'index',
            'data' => $content['data'],
        ]);
    }
}